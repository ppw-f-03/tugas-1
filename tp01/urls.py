"""tp01 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
from django.conf.urls import url
from SinarPerak.views import *
from News.views import *
from Registration.views import *
from Donate.views import *
from Programs.views import *
from Login.views import *
from listDonasi.views import *

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('SinarPerak.urls')),
    path('Donate/', include('Donate.urls')),
    path('registration/',include('Registration.urls')),
    path('news/', include('News.urls')),
    path('programs/', include('Programs.urls')),
    path('login/', include('Login.urls')),
    path('about/', include('About.urls')),
    path('daftar-donasi/',include('listDonasi.urls')),
]

