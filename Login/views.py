from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
# from django.contrib.auth.views import logout
from django.contrib.auth import logout
# import requests, json

# Create your views here.
def login(request):
    if request.user.is_authenticated:
        return redirect('/login/logout_page')
    return render(request, 'login-page.html')

def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/login') 

def logout_view_page(request):
    return render(request, 'logout-page.html')




