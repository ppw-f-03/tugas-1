from django.urls import path, include
from django.conf.urls import url
from django.contrib.auth import views
from . import views
#url for app
urlpatterns = [
    path('', views.login, name='login'),
    path('logout', views.logout_view, name='logout'),
    path('logout_page', views.logout_view_page, name='logout_page'),
    path('auth', include('social_django.urls', namespace='social')),  # <- Here
]
