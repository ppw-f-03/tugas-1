from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.apps import apps
from Login.apps import LoginConfig
from django.http import HttpRequest
from django.contrib.auth.models import User
from .views import *
import unittest


# Create your tests here.
class LoginConfigTest(TestCase):
    def test_apps(self):
        self.assertEqual(LoginConfig.name, 'Login')
        self.assertEqual(apps.get_app_config('Login').name, 'Login')

class TPLoginUnauthenticatedUnitTest(TestCase):    
    def test_login_page_exists(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code,200)

    def test_login_contains_signin_button(self):
        response = Client().get('/login/')
        html_response = response.content.decode('utf8')
        self.assertIn("Login/img/google_signin.png", html_response) 

    def test_login_using_login_function(self):
        found = resolve('/login/')
        self.assertEqual(found.func, login)

    def test_logout_page_exists(self):
        response = Client().get('/login/logout_page')
        self.assertEqual(response.status_code,200)

    def test_logout_page_doesnt_contains_signout_button_if_user_unauthenticated(self):
        response = Client().get('/login/logout_page')
        html_response = response.content.decode('utf8')
        self.assertIn("ANDA BELUM MASUK MENGGUNAKAN AKUN ANDA", html_response)

    def test_logout_page_using_logout_page_function(self):
        found = resolve('/login/logout_page')
        self.assertEqual(found.func, logout_view_page)

    def test_logout_function_redirects_after_logged_out(self):
        response = Client().get('/login/logout')
        self.assertEqual(response.status_code,302)

    def test_logout_using_logout_function(self):
        found = resolve('/login/logout')
        self.assertEqual(found.func, logout_view) 

class TPLoginAuthenticatedUnitTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.u = User.objects.create_user(username='testclient', password='password', is_active=True)

    def test_login_redirects_to_logout_if_user_authenticated(self):
        self.client.force_login(self.u)
        response = self.client.get('/login/')
        self.assertEqual(response.status_code,302)

    def test_logout_contains_signout_greetings(self):
        self.client.force_login(self.u)
        response =self.client.get('/login/logout_page')
        html_response = response.content.decode('utf8')
        self.assertIn("KELUAR DARI AKUN ANDA?", html_response)


    

