from django.test import TestCase
from django.test import Client
from django.utils.timezone import now
from .views import *
from .forms import testimonie_form
from .models import Testimonies
from django.contrib.auth.models import User

import os

stringMax = 'teststeststeststeststeststeststeststeststeststeststeststeststeststeststeststeststeststeststeststeststeststeststests'

class AboutTest(TestCase):
 
    def test_if_about_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_if_tentang_kami_exist(self):
        test = "Tentang Kami"
        response= Client().get('/about/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_if_testimonie_draft_exist(self):
        test = 'Apa yang <b style="color: black">Donatur</b> kami katakan'
        response= Client().get('/about/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_form_validation_max_error(self):
        form = testimonie_form(data={'testimonie' : stringMax})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['testimonie'],
            ["Maksimal karakter hanya 100"]
        )

    def test_if_models_is_ok(self):
        new_testimonie = Testimonies.objects.create(username='some donatur', testimonie='sangat hebat!')
        counting_all_available_todo = Testimonies.objects.all().count()
        self.assertEqual(counting_all_available_todo, 1)

    def test_validation_form(self):
        form = testimonie_form(data={'testimonie' : 'some_string'})
        self.assertTrue(form.is_valid())

class UserAuthenticationUnitTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.u = User.objects.create_user(username='testclient', password='password', is_active=True)

    def test_if_login_succes_and_render_the_form(self):
        self.client.force_login(self.u)
        response = self.client.get('/about/')
        html_response = response.content.decode('utf8')
        self.assertIn("Bagaimana dengan pendapat anda, ", html_response)
        self.assertIn("submit", html_response)

       
        
        




