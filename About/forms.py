from django import forms
from .models import Testimonies

class testimonie_form(forms.ModelForm):
	testimonie = forms.Field(
				widget = forms.TextInput(attrs={'class':'form-control',
												'type': 'text',
												'placeholder':'Tulis di sini pendapat anda mengenai kami',
												'id':'input-testimonie'}),
				label="",
        		error_messages={'max_length': 'Maksimal karakter hanya 100'},)
	class Meta:
		model = Testimonies
		fields = ['testimonie', ]