var counter = 1;
var testCount = 0;
var new_test_counter = 0;
$(function() {
	$('#form-testimonie').on('submit', function(e) {
	    e.preventDefault();
	    var testimonie = $('#input-testimonie').val();
	    var username = $('#username_temp').text();
	    var data = {
	      'testimonie': testimonie,
	      'username': username,
	    };
	    var lines = $('#body');
	    testCount = testCount+1;
	    counter = counter+1;
	    $.ajax( {
	    	type: 'POST',
	    	url: '/about/',
	      	data: data,
	      	success: function (data) {
	    		new_test_counter++;
	    		console.log(new_test_counter);
	    		var newTest = $('<div id="new_testimonie'+new_test_counter+'"></div>')
			    var newLine = $('<div style="margin-bottom: 1em;" id="line'+counter+'">'+
									'<div id="content" style="color: white; text-align: center;">'+
										'<p id="testimonie_donatur'+testCount+'">'+'"'+testimonie+'"'+'</p>'+
										'<p id="name_donatur'+testCount+'"><b>'+'<span style="color: #ccc;">'+'-'+username+'-'+'</span>'+'</b></p>'+
									'</div>'+
								'</div>');
			    if (testimonie.length <= 100) {
			    	$('#error_message').text('');
			    	$("#new_testimonie").append(newTest);
			    	$("#new_testimonie"+new_test_counter+"").append(newLine);
			    }
			    else {
			    	$('#error_message').text('Maksimal karakter hanya 100');
			    }
			    $('#input-testimonie').val('');
	      	},
	      	error: function(result) {
	      		console.log('some error has occured !');
	      	}
	   	});
	});

	var old_count = 0;

	$.ajax( {
		type: 'GET',
		url: '/about/data/'
	}).done(function(data) {
		$(data.testimonies.reverse()).each(function(i, x) {
			// console.log(i);
			old_count = old_count + 1;
			if (old_count > 8) {
				user_name = '';
				content = '';
				var newLine = $('');
			}
			else {
				user_name = x.username;
				content = x.content;
				var newLine = $('<div style="margin-bottom: 1em;" id="line'+counter+'">'+
									'<div id="content" style="color: white; text-align: center;">'+
										'<p id="testimonie_donatur'+testCount+'">'+'"'+content+'"'+'</p>'+
										'<p id="name_donatur'+testCount+'"><b>'+'<span style="color:#ccc">'+'-'+user_name+'-'+'</span>'+'</b></p>'+
									'</div>'+
								'</div>');
			}
	    	$('#error_message').text('');
	    	$('#old_testimonie').append(newLine);
	    	counter = counter+1;
		});
		counter--;
	});
});