from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect, JsonResponse, HttpResponse
from django.contrib.auth.models import User
from .models import Testimonies
from . import forms
from django.views.decorators.csrf import csrf_exempt

import json 

# Create your views here.
response={}

@csrf_exempt
def about(request):
	if request.user.is_authenticated:
		username = ('%s %s') % (request.user.first_name, request.user.last_name)
		response['User'] = username

	response['comments'] = Testimonies.objects.all()

	if request.method == 'POST':
		form = forms.testimonie_form(request.POST)
		if form.is_valid():
			data = Testimonies(username=username, testimonie=request.POST.get('testimonie'))
			data.save()
	else:
		form = forms.testimonie_form()
		response['form'] = form

	return render(request, 'about.html', response)

def data(request):
	response = {}
	testimonies = Testimonies.objects.all()
	response = [{'username':testimoni.username, 'content':testimoni.testimonie} for testimoni in testimonies]
	return HttpResponse(json.dumps({'testimonies': response}),
                        content_type='application/json')