from django.urls import path
from . import views


urlpatterns = [
    path('about', views.about, name='about'),
    path('about/', views.about, name='about'),
    path('data/', views.data, name='data'),
    path('', views.about, name='about')
]