from django.db import models
from datetime import date
from django.urls import reverse #Used to generate URLs by reversing the URL patterns

# Create your models here.

class Berita(models.Model):
    """
    Model representing a news post.
    """
    author = models.CharField(max_length=200)
    title = models.CharField(max_length=200)
    description = models.CharField(max_length=250, default="INDONESIA KEMBALI BERDUKA")
    picture = models.CharField(max_length=300)
    post_date = models.DateField(default=date.today)
    news = models.TextField(max_length=5000, help_text="Enter you news text here.")
    
    class Meta:
        ordering = ["-post_date"]
    
    def get_absolute_url(self):
        """
        Returns the url to access a particular news instance.
        """
        return reverse('detail_berita', args=[str(self.id)])

    def __str__(self):
        """
        String for representing the Model object.
        """
        return self.title

