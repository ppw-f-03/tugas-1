from django.test import TestCase
from .models import Berita
from django.urls import reverse
import datetime
# Create your tests here.

class BeritaModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        Berita.objects.create(author='Fulan',title='Test Blog',description='Test Blog Description', picture='https://via.placeholder.com/350x150', news='test')
    
    def test_get_absolute_url(self):
        news=Berita.objects.get(pk=1)       
        self.assertEquals(news.get_absolute_url(),'/news/news/1')
           
    def test_title_label(self):
        news=Berita.objects.get(pk=1)
        field_label = news._meta.get_field('title').verbose_name
        self.assertEquals(field_label,'title')
        
    def test_name_max_length(self):
        news=Berita.objects.get(pk=1)
        max_length = news._meta.get_field('title').max_length
        self.assertEquals(max_length,200)
        
    def test_description_label(self):
        news=Berita.objects.get(pk=1)
        field_label = news._meta.get_field('description').verbose_name
        self.assertEquals(field_label,'description')
        
    def test_description_max_length(self):
        news=Berita.objects.get(pk=1)
        max_length = news._meta.get_field('description').max_length
        self.assertEquals(max_length,250)

    def test_date_label(self):
        news=Berita.objects.get(pk=1)
        field_label = news._meta.get_field('post_date').verbose_name
        self.assertEquals(field_label,'post date')
        
    def test_date(self):
        news=Berita.objects.get(pk=1)
        the_date = news.post_date
        self.assertEquals(the_date,datetime.date.today())

    def test_object_title(self):
        news=Berita.objects.get(pk=1)
        expected_object_title = news.title
        self.assertEquals(expected_object_title,str(news))

class BeritaListView(TestCase):
    @classmethod
    def setUpTestData(cls):
        number_of_news = 13
        for news_num in range(number_of_news):
           Berita.objects.create(author='Fulan',title='Test Blog %s' % news_num,description='Test Blog %s Description' % news_num, picture='https://via.placeholder.com/350x150'
           , news='test')
    
    def test_view_url_exists_at_desired_location(self): 
        resp = self.client.get('/news/') 
        self.assertEqual(resp.status_code, 200)  
           
    def test_view_url_accessible_by_name(self):
        resp = self.client.get(reverse('news'))
        self.assertEqual(resp.status_code, 200)
        
    def test_view_uses_correct_template(self):
        resp = self.client.get(reverse('news'))
        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'list.html')