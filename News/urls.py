from django.urls import path
from . import views


urlpatterns = [
    path('news', views.list_berita, name='news'),
    path('news/', views.list_berita, name='news'),
    path('', views.list_berita, name='news'),
    path('news/<int:pk>', views.detail_berita, name='detail_berita'),
]