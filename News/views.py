from django.shortcuts import render
from .models import Berita
# Create your views here.

response = {}
def detail_berita(request, pk):
    obj = Berita.objects.get(pk=pk)
    response['berita'] = obj
    return render(request, 'detail.html', response)

def list_berita(request):
    obj = Berita.objects.all().order_by('-id')
    response['list'] = obj
    return render(request, 'list.html', response)


