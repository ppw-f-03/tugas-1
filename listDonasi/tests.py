from django.test import TestCase,Client
from Donate.models import Formulir
from django.contrib.auth.models import User
#Create your tests here.
class ListDonasiTestApp(TestCase):
    def test_url_is_exist_logged_in(self):
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        response = self.client.get('/daftar-donasi/') 
        self.assertEquals(response.status_code,200)
    def test_url_is_exist_not_logged_in(self):
        response = self.client.get('/daftar-donasi/')
        self.assertRedirects(response, '/login/')
    def test_html_is_containt(self):
        self.user = User.objects.create_user(username='testuser', password='12345')
        login = self.client.login(username='testuser', password='12345')
        response = self.client.get('/daftar-donasi/')
        html_response = response.content.decode('utf8')
        self.assertIn("Mau Berdonasi?",html_response)