from django.shortcuts import render
from django.contrib.auth.models import User
from django.http import JsonResponse, HttpResponseRedirect
from Donate.models import *

def donatur(request):
    if request.user.is_authenticated:
        response = {}
        obj = request.user
        response['donasi'] = Formulir.objects.all().filter(email = obj.email)
        response['User'] = obj.get_full_name()
        return render(request, 'profil.html',  response)
    else:
        return HttpResponseRedirect('/login/')
def listDonasi(request):
    donatur = request.user
    semuaDonasi = Formulir.objects.all().filter(email = donatur.email)
    response = {"data": [{'nama_program':donasi.namaProgram, 'jumlah':donasi.jumlahDonasi} for donasi in semuaDonasi]}
    return JsonResponse(response)

# Create your views here.