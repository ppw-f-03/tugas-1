from django.db import models
# from Program.models import program

# Create your models here.
class Formulir(models.Model):
    namaProgram = models.CharField(max_length=100)
    namaLengkap = models.CharField(max_length=40, blank=True) 
    email = models.EmailField()
    jumlahDonasi = models.CharField(max_length=100)
    is_anon = models.BooleanField()

    def __str__(self):
        return self.namaProgram


