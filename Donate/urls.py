from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('<int:pk>', views.formul, name = 'form'),
    # path('', views.formul, name = 'form'),
    path('thankyou', views.thankyou, name = 'thankyou'),
]