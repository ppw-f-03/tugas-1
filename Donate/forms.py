from django import forms
from .models import Formulir

class Formulir_Form(forms.Form): 
    invalid_message={
        'required' : 'Please fill out this field',
        'invalid' : 'Please fill out with valid input',
    }
    # namaProgram_attrs={
    #     'type' : 'text',
    #     'class' : 'todo-form-input',
    # }
    # namaLengkap_attrs={
    #     'type' : 'text',
    #     'class' : 'todo-form-input',
    # }
    # email_attrs={
    #     'type' : 'email',
    #     'class' : 'todo-form-input',
    # }
    jumlahDonasi_attrs={
        'type' : 'number',
        'min' : '5000',
        'class' : 'todo-form-input',
    }


    # namaProgram =  forms.CharField(label='nama program', max_length=100, required=True,widget=forms.TextInput(attrs = namaProgram_attrs))
    # namaLengkap = forms.CharField(label='nama lengkap', max_length=20, required=False, widget=forms.TextInput(attrs = namaLengkap_attrs)) 
    # email = forms.EmailField(label='email', required=True, widget=forms.EmailInput(attrs = email_attrs))
    jumlahDonasi = forms.CharField(label='JUMLAH DONASI', max_length=100, required=True, widget=forms.TextInput(attrs = jumlahDonasi_attrs))
    is_anon = forms.BooleanField(widget=forms.CheckboxInput, required=False, label="DONASI SEBAGAI ANONIM")



    def clean(self):
        cleaned_data = super().clean()
        # namaLengkap = cleaned_data.get("namaLengkap")
        # is_anon = cleaned_data.get("is_anon")
        # if not is_anon and not namaLengkap:
        #     raise forms.ValidationError(
        #         "Nama kosong"
        #     )

    