from django.shortcuts import render
from django.contrib.auth.decorators import login_required

# Create your views here.
from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import Formulir_Form
from .models import Formulir
from Registration.models import Registration
from Programs.models import Program
from django.contrib.auth.models import User

response={}

@login_required
def formul(request, pk):
    form = Formulir_Form(request.POST or None)
    response={}
    donasi1 = Formulir.objects.all()
    prog = Program.objects.get(pk=pk)
    response['nama_program'] = prog
    response['form'] = form
    namaLengkap = ('%s %s') % (request.user.first_name, request.user.last_name)
    email = request.user.email
    response['nama_lengkap'] = namaLengkap
    response['email_user'] = email
    namaProgram = prog.full_title
    # namaProgram = prog.full_title
    if(request.method == 'POST'):
        if(form.is_valid()):
            print("valid")
            # try :
            #     user = Registration.objects.get(email=email)
            # except :
            #     return redirect('registration')
            jumlahDonasi = request.POST.get("jumlahDonasi")
            is_anon = request.POST.get("is_anon")
            # donasi2 = Formulir(namaProgram=namaProgram, namaLengkap=namaLengkap, email=email, jumlahDonasi=jumlahDonasi, is_anon=is_anon)
            if is_anon:
                donasi2 = Formulir(namaProgram=namaProgram, namaLengkap=namaLengkap, email=email, jumlahDonasi=jumlahDonasi, is_anon=True)
            else :
                donasi2 = Formulir(namaProgram=namaProgram, namaLengkap=namaLengkap, email=email, jumlahDonasi=jumlahDonasi, is_anon=False)
            donasi2.full_clean()
            donasi2.save()
            return redirect('thankyou')
        else :
            print("not valid")
            return render(request, 'form.html', response)
    else :
        # prog = program.objects.get(id=request.GET['id'])
        # response['nama_program'] = prog.nama_program
        response['form'] = form
        return render(request, 'form.html', response)

@login_required
def thankyou(request):
    return render(request, "thankyou.html", {})

        

