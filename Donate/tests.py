from django.test import TestCase

# Create your tests here.
from django.test import Client
from .models import Formulir
from .forms import Formulir_Form
from django.urls import resolve
from django.http import HttpRequest
import unittest
from .models import Formulir
from .forms import Formulir_Form
from .views import formul
from .views import thankyou
from Programs.models import Program


class Lab6_Test(TestCase):

    def test_Donate_formulir_donasi_using_pendaftaran_func(self):
        new_program = Program.objects.create(
        name = "Bencana",
        description ="bencana alam",
        full_title = "bencana",
        date_opened = "2018-12-08",
        date_closed = "2018-12-09",
        pic = "https://statik.tempo.co/data/2018/10/12/id_740268/740268_720.jpg")
        found = resolve('/Donate/1')
        self.assertEqual(found.func, formul)

    def test_Donate_anon_UserForm_valid(self):
        form = Formulir_Form(data={'namaLengkap':"",'email': "user@yahoo.com" , 'jumlahDonasi': "12000", 'is_anon': True})
        self.assertTrue(form.is_valid())

    def test_Donate_UserForm_invalid(self):
        form = Formulir_Form(data={'namaLengkap':'', 'email': '' , 'jumlahDonasi': '', 'is_anon': False})
        self.assertFalse(form.is_valid())

    def test_Donate_model_can_create_new_user(self):
        new_user = Formulir.objects.create(namaProgram="aaa",namaLengkap="me", email="itsme@yahoo.com", jumlahDonasi=12000, is_anon=False)
        counting_all_user = Formulir.objects.all().count()
        self.assertEqual(counting_all_user, 1)

    def test_Donate_form_validation_for_blank_items(self):
        form = Formulir_Form(data={'form': ''})
        self.assertFalse(form.is_valid())

    


