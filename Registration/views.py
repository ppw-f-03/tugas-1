from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Registration
from .forms import RegistrationForm
from django.contrib import messages

# Create your views here.


def registration(request):
    if request.method == 'POST':
        form = RegistrationForm(request.POST)
        if form.is_valid():
            email = request.POST['email']
            emailAda = Registration.objects.all().filter(email = email)
            if len(emailAda) == 0:
                form.save()
                return HttpResponseRedirect('/')
            else:
                messages.error(request,"Email telah terdaftar")
    elif request.method == 'GET':
        form = RegistrationForm()
    return render (request, 'registration.html', {'form' : form})
