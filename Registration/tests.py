from django.test import TestCase
from django.test import Client
from .models import Registration
from .forms import RegistrationForm

class RegistrationTestApp(TestCase):
    def test_url_is_exist(self):
        response = Client().get('/registration/')
        self.assertEquals(response.status_code,200)

    def test_form_submit_success(self):
        testEmail = 'm.nurfaizh@gmail.com'
        testNomorHp = '081210243341'
        testPassword = 'qwerty'
        testNamaLengkap = 'muhammad nur faiz habibullah'
        testNamaPanggilan = 'faiz'
        response_post = Client().post('/registration/', {'nama_lengkap': testNamaLengkap, 'nomor_hp':testNomorHp,
        'nama_panggilan':testNamaPanggilan,'email':testEmail,'kata_sandi':testPassword,})
        self.assertEqual(response_post.status_code, 302)

    def test_form_submit_failed(self):
        testEmail = ''
        testNomorHp = ''
        testPassword = ''
        testNamaLengkap = ''
        testNamaPanggilan = ''
        response_post = Client().post('/registration/', {'nama_lengkap': testNamaLengkap, 'nomor_hp':testNomorHp,
        'nama_panggilan':testNamaPanggilan,'email':testEmail,'kata_sandi':testPassword,})
        self.assertEqual(response_post.status_code, 200)

    def test_form_containt_all(self):
        response = Client().get('/registration/')
        html_response = response.content.decode('utf8')
        self.assertIn('Nama Lengkap:', html_response)
        self.assertIn('Nama Panggilan:', html_response)
        self.assertIn('Nomor HP:',html_response)
        self.assertIn('Email:',html_response)
        self.assertIn('Kata Sandi',html_response)
# Create your tests here.
