from django.db import models
# Create your models here.
class Registration(models.Model):
    nama_lengkap = models.CharField(max_length = 100)
    nama_panggilan = models.CharField(max_length = 30)
    email = models.CharField(max_length = 1000)
    nomor_hp = models.IntegerField()
    kata_sandi = models.CharField(max_length = 1000)

    def __str__(self):
        return self.nama_lengkap