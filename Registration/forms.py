from django import forms
from .models import Registration

class RegistrationForm(forms.ModelForm):
    nama_lengkap = forms.Field(
        widget = forms.TextInput(
            attrs={'class':'form-control','type': 'text','placeholder':'Nama Lengkap'}
        ),label='Nama Lengkap',
        error_messages={'max_length': 'Maksimal karakter hanya 100'}
    )
    nama_panggilan = forms.Field(
        widget = forms.TextInput(
            attrs={'class':'form-control','type': 'text','placeholder':'Nama Panggilan'}
        ),label='Nama Panggilan',
        error_messages={'max_length': 'Maksimal karakter hanya 30'}
    )
    nomor_hp = forms.IntegerField(
        widget = forms.TextInput(
            attrs={'class':'form-control','type': 'text','placeholder':'Nomor HP'}
        ),label='Nomor HP',
    )
    email = forms.Field(
        widget = forms.TextInput(
            attrs={'class':'form-control','type': 'email','placeholder':'Email'}
        ),label='Email',
    )
    kata_sandi = forms.Field(
        widget = forms.TextInput(
            attrs={'class':'form-control','type': 'password','placeholder':'Kata Sandi'}
        ),label='kata sandi',
    )
    class Meta:
        model = Registration
        fields = ['nama_lengkap','nama_panggilan','nomor_hp','email','kata_sandi',]