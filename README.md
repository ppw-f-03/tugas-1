# Tugas Pemrograman 1 PPW 2018
Kelompok 3

* * *

# Nama Anggota - NPM:
1. Muhammad Nur Faiz Habibullah - 1706043456
2. Alaya Khairunnisa - 1706043733
3. Reinaldy Rabbany - 1706043872
4. Muhammad Andriansyah Putra - 1706044093

* * *

# Link Herokuapp
Link: https://tp1-ppw-f-03.herokuapp.com

* * *

# Link Gitlab
Link: https://gitlab.com/ppw-f-03/tugas-1.git

* * *

# Pipeline Status
[![pipeline status](https://gitlab.com/ppw-f-03/tugas-1/badges/master/pipeline.svg)](https://gitlab.com/ppw-f-03/tugas-1/badges/master/pipeline.svg)

* * *

# Code Coverage Report

[![coverage report](https://gitlab.com/ppw-f-03/tugas-1/badges/master/coverage.svg)](https://gitlab.com/ppw-f-03/tugas-1/commits/master)

* * *





