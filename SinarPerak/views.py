from django.shortcuts import render
from News.models import Berita
from Programs.models import Program

# Create your views here.
response = {}
def index(request):
    html = 'homepage.html'
    obj1 = Berita.objects.all().order_by('-id')[:3]
    obj2 = Program.objects.all().order_by('-id')[:3]
    response['list_news'] = obj1
    response['list_prog'] = obj2
    return render(request, 'homepage.html', response)
