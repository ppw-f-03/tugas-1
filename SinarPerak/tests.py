from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index

# Create your tests here.
class SinarPerakHomeUnitTest(TestCase):
    
    def test_hello_page_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    '''def test_index_contains_hello_apa_kabar(self):
        response = Client().get('/')
        html_response = response.content.decode('utf8')
        self.assertIn("Hello World!", html_response)      '''


