from django.urls import path
from . import views

urlpatterns = [
    path('list', views.program_list, name='program'),
    path('list/', views.program_list, name='program'),
    path('', views.program_list, name='program'),
    path('list/<int:pk>', views.detail_program, name='detail_program'),
]
