from django.db import models
from datetime import date
from django.urls import reverse #Used to generate URLs by reversing the URL patterns

# Create your models here.

class Program(models.Model):
    name = models.CharField(max_length=200, default="")
    description = models.CharField(max_length=200, default="")
    full_title = models.CharField(max_length=200, default="")
    date_opened = models.DateField(default=date.today)
    date_closed = models.DateField(default=date.today)
    pic = models.CharField(max_length=200, default="")

    class Meta:
        ordering = ["-date_closed"]
    
    def get_absolute_url(self):
	    return reverse('detail_program', args=[str(self.id)])

    def __str__(self):
        return self.full_title