from django.shortcuts import render
from .models import Program
from Donate.models import Formulir
# Create your views here.

response = {}
def program_list(request):
    obj = Program.objects.all().order_by('-id')
    response['prog'] = obj
    return render(request, 'programs.html', response)

def detail_program(request, pk):
    obj = Program.objects.get(pk=pk)
    response['donors'] = Formulir.objects.all().filter(namaProgram = obj.full_title)
    response['program'] = obj
    return render(request, 'program_detail.html', response)
